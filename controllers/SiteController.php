<?php

namespace app\controllers;

use app\models\Article;
use app\models\Category;
use app\models\CommentForm;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /* iNICIO */
    public function actionIndex()
    {
        $data = Article::getAll(5);
        $popular = Article::getPopular();
        $reciente = Article::getRecent();
        $categorias = Category::getAll();
        
        return $this->render('index',[
            'articles'=>$data['articles'],
            'paginacion'=>$data['paginacion'],
            'popular'=>$popular,
            'reciente'=>$reciente,
            'categorias'=>$categorias
        ]);
    }
    
    public function actionView($id)
    {
        $articulo = Article::findOne($id);
        $popular = Article::getPopular();
        $reciente = Article::getRecent();
        $categorias = Category::getAll();
        $comentarios = $articulo->getArticleComments();
        $comentariosForm = new CommentForm();

        $articulo->viewedCounter();
        
        return $this->render('single',[
            'articulo'=>$articulo,
            'popular'=>$popular,
            'reciente'=>$reciente,
            'categorias'=>$categorias,
            'comentarios'=>$comentarios,
            'comentariosForm'=>$comentariosForm
        ]);
    }
    
    public function actionCategory($id)
    {

        $data = Category::getArticlesByCategory($id);
        $popular = Article::getPopular();
        $reciente = Article::getRecent();
        $categorias = Category::getAll();
        
        return $this->render('category',[
            'articulos'=>$data['articulos'],
            'pagination'=>$data['pagination'],
            'popular'=>$popular,
            'reciente'=>$reciente,
            'categorias'=>$categorias
        ]);
    }

    public function actionComment($id)
    {
        $model = new CommentForm();
        
        if(Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if($model->saveComment($id))
            {
                Yii::$app->getSession()->setFlash('comment', 'Tu comentario se añadio correctamente!');
                return $this->redirect(['site/view','id'=>$id]);
            }
        }
    }

}
