<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article`.
 */
class m170124_021553_create_article_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('articulos', [
            'id' => $this->primaryKey(),
            'titulo'=>$this->string(),
            'descripcion'=>$this->text(),
            'contenido'=>$this->text(),
            'fecha'=>$this->date(),
            'imagen'=>$this->string(),
            'vistas'=>$this->integer(),
            'usuario_id'=>$this->integer(),
            'estado'=>$this->integer(),
            'categoria_id'=>$this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('articulos');
    }
}
