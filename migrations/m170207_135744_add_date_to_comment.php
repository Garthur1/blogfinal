<?php

use yii\db\Migration;

class m170207_135744_add_date_to_comment extends Migration
{
    public function up()
    {
        $this->addColumn('comentarios','fecha', $this->date());
    }

    public function down()
    {
        $this->dropColumn('comentarios','fecha');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
