<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m170124_021622_create_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('comentarios', [
            'id' => $this->primaryKey(),
            'texto'=>$this->string(),
            'usuario_id'=>$this->integer(),
            'articulo_id'=>$this->integer(),
            'estado'=>$this->integer()
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-post-user_id',
            'comentarios',
            'usuario_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-post-user_id',
            'comentarios',
            'usuario_id',
            'usuarios',
            'id',
            'CASCADE'
        );

        // creates index for column `article_id`
        $this->createIndex(
            'idx-article_id',
            'comentarios',
            'articulo_id'
        );

        // add foreign key for table `article`
        $this->addForeignKey(
            'fk-article_id',
            'comentarios',
            'articulo_id',
            'articulos',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('comentarios');
    }
}
