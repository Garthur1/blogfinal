<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article_tag`.
 */
class m170124_021633_create_article_tag_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('articulos_tag', [
            'id' => $this->primaryKey(),
            'articulo_id'=>$this->integer(),
            'tag_id'=>$this->integer()
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'tag_article_article_id',
            'articulos_tag',
            'articulo_id'
        );


        // add foreign key for table `user`
        $this->addForeignKey(
            'tag_article_article_id',
            'articulos_tag',
            'articulo_id',
            'articulos',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx_tag_id',
            'articulos_tag',
            'tag_id'
        );


        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-tag_id',
            'articulos_tag',
            'tag_id',
            'tags',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('articulos_tag');
    }
}
