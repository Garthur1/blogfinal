<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m170124_021601_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('categorias', [
            'id' => $this->primaryKey(),
            'titulo'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('categorias');
    }
}
