<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $texto
 * @property integer $usuario_id
 * @property integer $articulo_id
 * @property integer $estado
 *
 * @property Articulos $articulo
 * @property Usuarios $usuario
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const STATUS_ALLOW = 1;
    const STATUS_DISALLOW = 0;

    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'articulo_id', 'estado'], 'integer'],
            [['texto'], 'string', 'max' => 255],
            [['articulo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['articulo_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'texto' => 'Text',
            'usuario_id' => 'ID Usuario',
            'articulo_id' => 'Id Articulo',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'articulo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'usuario_id']);
    }

    public function getDate()
    {
        return Yii::$app->formatter->asDate($this->fecha);
    }
    
    public function isAllowed()
    {
        return $this->estado;
    }

    public function allow()
    {
        $this->estado = self::STATUS_ALLOW;
        return $this->save(false);
    }

    public function disallow()
    {
        $this->estado = self::STATUS_DISALLOW;
        return $this->save(false);
    }
}
