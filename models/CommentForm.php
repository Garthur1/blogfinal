<?php

namespace app\models;

use Yii;
use yii\base\Model;

class CommentForm extends Model
{
    public $comentario;
    
    public function rules()
    {
        return [
            [['comentario'], 'required'],
            [['comentario'], 'string', 'length' => [3,250]]
        ];
    }

    public function saveComment($articulo_id)
    {
        $comentario = new Comment;
        $comentario->texto = $this->comentario;
        $comentario->usuario_id = Yii::$app->user->id;
        $comentario->articulo_id = $articulo_id;
        $comentario->estado = 0;
        $comentario->fecha = date('Y-m-d');
        return $comentario->save();

    }
}