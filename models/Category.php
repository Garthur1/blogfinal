<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $titulo
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'titulo',
        ];
    }

    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['categoria_id' => 'id']);
    }

    public function getArticlesCount()
    {
        return $this->getArticles()->count();
    }
    
    public static function getAll()
    {
        return Category::find()->all();
    }
    
    public static function getArticlesByCategory($id)
    {
        // todos los articulos
        $query = Article::find()->where(['categoria_id'=>$id]);

        // numero de articulos
        $count = $query->count();

        // create a pagination object with the total count
        $paginacion = new Pagination(['totalCount' => $count, 'pageSize'=>6]);

        $articulos = $query->offset($paginacion->offset)
            ->limit($paginacion->limit)
            ->all();

        $data['articulos'] = $articulos;
        $data['pagination'] = $paginacion;
        
        return $data;
    }
}
