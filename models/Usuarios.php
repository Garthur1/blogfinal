<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $email
 * @property string $contrasenia
 * @property integer $esAdmin
 * @property string $foto
* @property string $auth_key
 * @property string $access_token
 * @property Comment[] $comments
 */
class Usuarios extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['esAdmin'], 'integer'],
            [['nombre', 'email', 'contrasenia', 'foto'], 'string', 'max' => 255],
            [['auth_key', 'access_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'email' => 'Email',
            'contrasenia' => 'Contrasena',
            'esAdmin' => 'Es Admin',
            'foto' => 'Foto',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['usuario_id' => 'id']);
    }

    public static function findIdentity($id)
    {
        return Usuarios::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($nombre)
    {
        return Usuarios::find()->where(['user' => $nombre])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public function validatePassword($contrasenia)
    {
        return \Yii::$app->security->validatePassword($contrasenia, $this->contrasenia);
    }

    public static function findByEmail($email)
    {
        return Usuarios::find()->where(['email'=>$email])->one();
    }


    
    public function create()
    {
        return $this->save(false);
    }
    

    public function getImage()
    {
        return $this->foto;
    }
}
