<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $titulo
 * @property string $descripcion
 * @property string $contenido
 * @property string $fecha
 * @property string $imagen
 * @property integer $vistas
 * @property integer $usuario_id
 * @property integer $estado
 * @property integer $categoria_id
 *
 * @property ArticleTag[] $articulosTag
 * @property Comments[] $comentarios
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articulos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['titulo','descripcion','contenido'], 'string'],
            [['fecha'], 'date', 'format'=>'php:Y-d-m'],
            [['fecha'], 'default', 'value' => date('Y-d-m')],
            [['titulo'], 'string', 'max' => 255],
            [['categoria_id'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'contenido' => 'Contenido',
            'fecha' => 'Fecha',
            'imagen' => 'Imagen',
            'vistas' => 'Vistas',
            'usuario_id' => 'ID Usuario',
            'estado' => 'Estado',
            'categoria_id' => 'ID Categoria',
        ];
    }

    public function saveArticle()
    {
        $this->usuario_id = Yii::$app->user->id;
        return $this->save(false);
    }

    public function saveImage($filename)
    {
        $this->imagen = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->imagen) ? '/uploads/' . $this->imagen : '/no-image.png';
    }

    public function deleteImage()
    {
        $imagenUploadModel = new ImageUpload();
        $imagenUploadModel->deleteCurrentImage($this->imagen);
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete();
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoria_id']);
    }

    public function saveCategory($categoria_id)
    {
        $category = Category::findOne($categoria_id);
        if($category != null)
        {
            $this->link('category', $category);
            return true;            
        }
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('articulos_tag', ['articulo_id' => 'id']);
    }
    
    public function getSelectedTags()
    {
         $selectedIds = $this->getTags()->select('id')->asArray()->all();
        return ArrayHelper::getColumn($selectedIds, 'id');
    }

    public function saveTags($tags)
    {
        if (is_array($tags))
        {
            $this->clearCurrentTags();

            foreach($tags as $tag_id)
            {
                $tag = Tag::findOne($tag_id);
                $this->link('tags', $tag);
            }
        }
    }

    public function clearCurrentTags()
    {
        ArticleTag::deleteAll(['articulo_id'=>$this->id]);
    }
    
    public function getDate()
    {
        return Yii::$app->formatter->asDate($this->fecha);
    }
    
    public static function getAll($pageSize = 5)
    {
        $query = Article::find();

        $count = $query->count();

        $paginacion = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);

        $articulos = $query->offset($paginacion->offset)
            ->limit($paginacion->limit)
            ->all();
        
        $data['articles'] = $articulos;
        $data['paginacion'] = $paginacion;
        
        return $data;
    }
    
    public static function getPopular()
    {
        return Article::find()->orderBy('vistas desc')->limit(3)->all();
    }
    
    public static function getRecent()
    {
        return Article::find()->orderBy('fecha asc')->limit(4)->all();
    }
    
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['articulo_id'=>'id']);
    }

    public function getArticleComments()
    {
        return $this->getComments()->where(['estado'=>1])->all();
    }
    
    public function getAuthor()
    {
        return $this->hasOne(Usuarios::className(), ['id'=>'usuario_id']);
    }
    
    public function viewedCounter()
    {
        $this->vistas += 1;
        return $this->save(false);
    }
}
