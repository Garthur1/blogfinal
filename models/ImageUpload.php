<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUpload extends Model{
    
    public $imagen;

    public function rules()
    {
        return [
            [['imagen'], 'required'],
            [['imagen'], 'file', 'extensions' => 'jpg,png']
        ];
    }


    public function uploadFile(UploadedFile $file, $currentImage)
    {
        $this->imagen = $file;

       if($this->validate())
       {
           $this->deleteCurrentImage($currentImage);
           return $this->saveImage();
       }

    }

    private function getFolder()
    {
        return Yii::getAlias('@web') . '/uploads/';
    }

    private function generateFilename()
    {
        return strtolower(md5(uniqid($this->imagen->baseName)) . '.' . $this->imagen->extension);
    }

    public function deleteCurrentImage($currentImage)
    {
        if($this->fileExists($currentImage))
        {
            unlink($this->getFolder() . $currentImage);
        }
    }

    public function fileExists($currentImage)
    {
        if(!empty($currentImage) && $currentImage != null)
        {
            return file_exists($this->getFolder() . $currentImage);
        }
    }

    public function saveImage()
    {
        $filename = $this->generateFilename();

        $this->imagen->saveAs($this->getFolder() . $filename);

        return $filename;
    }
}