<?php

namespace app\models;

use yii\base\Model;

class SignupForm extends Model
{
    public $nombre;
    public $email;
    public $contrasenia;
    
    public function rules()
    {
        return [
            [['nombre','email','contrasenia'], 'required'],
            [['nombre'], 'string'],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass'=>'app\models\Usuarios', 'targetAttribute'=>'email']
        ];
    }
    
    public function signup()
    {
        $user = new Usuarios();
        $user->email = $this->email;
        $user->nombre = $this->nombre;
        $user->auth_key = \Yii::$app->security->generateRandomString();
        $user->access_token = \Yii::$app->security->generateRandomString();
        $user->contrasenia = \Yii::$app->security->generatePasswordHash($this->contrasenia);

        if ($user->save()){
            return true;
        }

        \Yii::error("User was not saved: ".VarDumper::dumpAsString($user->errors));
        return false;
    }
}