<?php
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use buibr\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'contenido')->widget(CKEditor::className(), [
        'editorOptions' => [
        'preset' => 'full',
        'inline' => false,
        ]
        ]) 
        ?>
        <?= DatePicker::widget([
            'model' => $model,
            'attribute' => 'fecha',
            'language' => 'es',
            'size' => 'lg',
            'readonly' => true,
            'placeholder' => 'Elige una fecha',
            'clientOptions' => [
                'format' => 'L',
                'minDate' => '2015-08-10',
                'maxDate' => '2015-09-10',
            ],
        'clientEvents' => [
            'dp.show' => new \yii\web\JsExpression("function () { console.log('Funciona!'); }"),
        ],
    ]);?>
        <?= $form->field($model, 'fecha')->widget(
            DatePicker::className(), [
                'addon' => false,
                'size' => 'sm',
                'clientOptions' => [
                    'format' => 'L',
                    'stepping' => 30,
                ],
        ]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
