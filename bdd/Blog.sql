-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 28-02-2021 a las 22:26:53
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Blog`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descripcion` text DEFAULT NULL,
  `contenido` text DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `vistas` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `titulo`, `descripcion`, `contenido`, `fecha`, `imagen`, `vistas`, `usuario_id`, `estado`, `categoria_id`) VALUES
(29, 'Javascript', 'Una definición basica de que es Javascript.', '<h1 style=\"text-align:center\">Definicion de Javascript.</h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>JavaScript</strong> es un lenguaje de programaci&oacute;n o de secuencias de comandos que te permite implementar funciones complejas en p&aacute;ginas web, cada vez que una p&aacute;gina web hace algo m&aacute;s que sentarse all&iacute; y mostrar informaci&oacute;n est&aacute;tica para que la veas, muestra oportunas actualizaciones de contenido, mapas interactivos, animaci&oacute;n de Gr&aacute;ficos 2D/3D, desplazamiento de m&aacute;quinas reproductoras de v&iacute;deo, etc., puedes apostar que probablemente JavaScript est&aacute; involucrado. Es la tercera capa del pastel de las tecnolog&iacute;as web est&aacute;ndar.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Ejemplo:</h3>\r\n\r\n<pre>\r\n<code>const para = document.querySelector(&#39;p&#39;);\r\n\r\npara.addEventListener(&#39;click&#39;, updateName);\r\n\r\nfunction updateName() {\r\n  let name = prompt(&#39;Enter a new name&#39;);\r\n  para.textContent = &#39;Player 1: &#39; + name;\r\n}\r\n</code></pre>\r\n', '2021-02-01', NULL, 42, 5, NULL, NULL),
(31, 'PHP', 'Que es PHP?', '<h2>Una definici&oacute;n b&aacute;sica.</h2>\r\n\r\n<p>PHP (acr&oacute;nimo recursivo de PHP: Hypertext Preprocessor) es un lenguaje de c&oacute;digo abierto muy popular especialmente adecuado para el desarrollo web y que puede ser incrustado en HTML.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bien, pero &iquest;qu&eacute; significa realmente? Un ejemplo nos aclarar&aacute; las cosas:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Ejemplo</h3>\r\n\r\n<p><code><span style=\"color:#000000\">&lt;!DOCTYPE&nbsp;html&gt;<br />\r\n&lt;html&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&lt;head&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;title&gt;Ejemplo&lt;/title&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&lt;/head&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&lt;body&gt;<br />\r\n<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"color:#0000BB\">&lt;?php<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color:#007700\">echo&nbsp;</span><span style=\"color:#DD0000\">&quot;&iexcl;Hola,&nbsp;soy&nbsp;un&nbsp;script&nbsp;de&nbsp;PHP!&quot;</span><span style=\"color:#007700\">;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color:#0000BB\">?&gt;</span><br />\r\n<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&lt;/body&gt;<br />\r\n&lt;/html&gt;</span></code></p>\r\n', '2020-02-01', NULL, 2, 5, NULL, NULL),
(32, 'Java', 'Que es Java?', '<h2>Que es Java?</h2>\r\n\r\n<p style=\"text-align: justify;\">Java es un lenguaje de prop&oacute;sito general capaz de acometer todo tipo de proyectos y ejecutarse en m&uacute;ltiples plataformas. Aqu&iacute; aprender&aacute;s qu&eacute; es Java y a programar en este lenguaje con diversos manuales.</p>\r\n\r\n<p style=\"text-align: justify;\">Java es un lenguaje de programaci&oacute;n de prop&oacute;sito general, uno de los m&aacute;s populares y con mayores aplicaciones del panorama actual. Existen diversos &iacute;ndices de lenguajes de programaci&oacute;n y dependiendo el que tomemos como referencia puede considerarse el lenguaje m&aacute;s popular, o uno de los 3 m&aacute;s populares que existen en el mundo.</p>\r\n\r\n<p style=\"text-align: justify;\">Fue creado inicialmente por la compa&ntilde;&iacute;a Sun Microsystems que consigui&oacute; posicionar su lenguaje como uno de los m&aacute;s punteros y extendidos, debido sobre todo a su versatilidad y soporte pr&aacute;cticamente universal. Actualmente se encuentra en propiedad de Oracle, despu&eacute;s que &eacute;sta adquiriera a Sun.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Java es multiplataforma</strong>, capaz de ejecutarse en la mayor&iacute;a de sistemas operativos y dispositivos, con una &uacute;nica base de c&oacute;digo. Esto lo consigue gracias a una m&aacute;quina virtual que existe en cada sistema que es capaz de ejecutar Java y hacer de puente entre el lenguaje de programaci&oacute;n y el dispositivo. Eso quiere decir que, si hacemos un programa en Java podr&aacute; funcionar en cualquier ordenador, dispositivo o cualquier tipo de m&aacute;quina que soporte Java.</p>\r\n\r\n<p style=\"text-align: justify;\">El hecho de ser multiplataforma es una ventaja significativa para los desarrolladores de software, pues anteriormente era necesario hacer un programa para cada sistema operativo, por ejemplo Windows, Linux, MacOS, etc. Esto lo consigue porque se ha creado una M&aacute;quina virtual de Java para cada plataforma, que hace de puente entre el sistema operativo y el programa de Java y posibilita que este &uacute;ltimo se entienda perfectamente. Hoy hay muchos lenguajes multiplataforma, pero Java fue de los primeros en ofrecer esta posibilidad.</p>\r\n\r\n<p style=\"text-align: justify;\">Con Java podemos hacer todo tipo de proyectos, desde aplicaciones web a servicios web basados en SOAP o REST, aplicaciones de escritorio de consola o interfaz gr&aacute;fica. Adem&aacute;s Java es el lenguaje de programaci&oacute;n que se usa para el desarrollo nativo para Android, lo que ha llevado a esta tecnolog&iacute;a a un nivel de popularidad todav&iacute;a mayor y con alta demanda porfesional</p>\r\n', '2020-02-01', NULL, NULL, 5, NULL, NULL),
(33, 'MySQL', 'Que es MySQL', '<h3 style=\"text-align:justify\">&iquest;Qu&eacute; es el programa MySQL?</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><strong>MySQL es un sistema de gesti&oacute;n de bases de datos que cuenta con una doble licencia</strong>. Por una parte es de c&oacute;digo abierto, pero por otra, cuenta con una versi&oacute;n comercial gestionada por la compa&ntilde;&iacute;a Oracle. Actualmente, es la base de datos de c&oacute;digo abierto m&aacute;s famosa y utilizada en el mundo entero.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<h6 style=\"text-align:justify\"><img alt=\"MySQL Workbench 8.0.23 para Windows - Descargar\" class=\"n3VNCb\" src=\"https://img.utdstc.com/screen/1/mysql-workbench.jpg\" style=\"float:left; height:285.652px; margin:0px; width:438px\" /></h6>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<h6 style=\"text-align:justify\">&nbsp;</h6>\r\n\r\n<h6 style=\"text-align:justify\">&nbsp;</h6>\r\n\r\n<h6 style=\"text-align:justify\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; IMAGEN SISTEMA GESTOR BDD WORKBENCH</h6>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2020-02-01', NULL, 2, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos_tag`
--

CREATE TABLE `articulos_tag` (
  `id` int(11) NOT NULL,
  `articulo_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `titulo`) VALUES
(1, 'javascript');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `texto` varchar(255) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `articulo_id` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id`, `texto`, `usuario_id`, `articulo_id`, `estado`, `fecha`) VALUES
(10, 'este es el primer post.', 5, 29, 1, '2021-02-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1614434094),
('m170124_021553_create_article_table', 1614434097),
('m170124_021601_create_category_table', 1614434097),
('m170124_021608_create_tag_table', 1614434097),
('m170124_021613_create_user_table', 1614434097),
('m170124_021622_create_comment_table', 1614434098),
('m170124_021633_create_article_tag_table', 1614434098),
('m170207_135744_add_date_to_comment', 1614434098);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tags`
--

INSERT INTO `tags` (`id`, `titulo`) VALUES
(1, 'Javascript'),
(2, 'PHP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contrasenia` varchar(255) DEFAULT NULL,
  `esAdmin` int(11) DEFAULT 0,
  `foto` varchar(255) DEFAULT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `contrasenia`, `esAdmin`, `foto`, `auth_key`, `access_token`) VALUES
(5, 'pablo', 'pggarcc@gmail.com', '$2y$13$hcJ5oLffjIeMgg3IymdRYuf5v1SvAhm/RirVDsaytNm7WbiR0/Is.', 1, NULL, '6AnxmfrVT2B3CiqdW0g8OcCel4rAgrjI', 'jgjrhyipIXVvv6HpPN_5hvvScomimeRg'),
(6, 'Juan', 'juan12@gmail.es', 'juanito123', 0, NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articulos_tag`
--
ALTER TABLE `articulos_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_article_article_id` (`articulo_id`),
  ADD KEY `idx_tag_id` (`tag_id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-post-user_id` (`usuario_id`),
  ADD KEY `idx-article_id` (`articulo_id`);

--
-- Indices de la tabla `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `articulos_tag`
--
ALTER TABLE `articulos_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulos_tag`
--
ALTER TABLE `articulos_tag`
  ADD CONSTRAINT `fk-tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tag_article_article_id` FOREIGN KEY (`articulo_id`) REFERENCES `articulos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `fk-article_id` FOREIGN KEY (`articulo_id`) REFERENCES `articulos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-post-user_id` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
